import streamlit as st
import asyncio
from autogen import AssistantAgent, UserProxyAgent
from autogen import get_config_list
import autogen
from openai.types import Model, ModelDeleted
from openai import OpenAI
import autogen
from autogen.agentchat.contrib.retrieve_assistant_agent import RetrieveAssistantAgent
from autogen.agentchat.contrib.retrieve_user_proxy_agent import RetrieveUserProxyAgent
from autogen.agentchat.contrib.retrieve_user_proxy_agent import RetrieveUserProxyAgent
from autogen import AssistantAgent
import chromadb


st.write("""# AutoGen Chat Agents""")

class StreamlitAssistantAgent(AssistantAgent):
    def _process_received_message(self, message, sender, silent):
        with st.chat_message(message['name']):
            st.markdown(message['content'])
        return super()._process_received_message(message, sender, silent)


class StreamlitUserProxyAgent(UserProxyAgent):
    def _process_received_message(self, message, sender, silent):
        with st.chat_message(message['name']):
            st.markdown(message['content'])

        return super()._process_received_message(message, sender, silent)

class StreamlitRetrieveUserProxyAgent(RetrieveUserProxyAgent):
    def _process_received_message(self, message, sender, silent):
        with st.chat_message(message['name']):
            st.markdown(message['content'])

        return super()._process_received_message(message, sender, silent)

selected_model = None
selected_key = None
with st.sidebar:

    import os

    selected_key = os.getenv('OPENAI_API_KEY', '')  # Default to empty string if not set
    client = OpenAI(
        # defaults to os.environ.get("OPENAI_API_KEY")
        api_key=selected_key,
    )
    st.header("OpenAI Configuration")
    model_list_sync_page = client.models.list()
    model_list = [model.id for model in model_list_sync_page.data if 'gpt' in model.id]
    st.write(model_list)
    selected_model = st.selectbox("Model", model_list, index=model_list.index('gpt-4-1106-preview'))

    base_urls = None  # You can specify API base URLs if needed. eg: localhost:8000
    api_type = "openai"  # Type of API, e.g., "openai" or "aoai".
    api_version = None  # Specify API version if needed.

    config_list = autogen.get_config_list(
        [selected_key],
        base_urls=base_urls,
        api_type=api_type,
        api_version=api_version
    )

  #  st.write(config_list)
    if not selected_key:
        st.error("OpenAI API key not found. Please set the OPENAI_API_KEY environment variable.")
        st.stop()

with st.container():
    # for message in st.session_state["messages"]:
    #    st.markdown(message)

    user_input = st.chat_input("Type something...")
    if user_input:
        if not selected_key or not selected_model:
            st.warning(
                'You must provide valid OpenAI API key and choose preferred model', icon="⚠️")
            st.stop()

        llm_config = {
            "timeout": 600,
            "config_list": [
                {
                    "model": selected_model,
                    "api_key": selected_key
                }
            ],
            "seed": 42,
            "functions": [
                {
                    "name": "retrieve_content",
                    "description": "retrieve content for custom question answering.",
                    "parameters": {
                        "type": "object",
                        "properties": {
                            "message": {
                                "type": "string",
                                "description": "Refined message which keeps the original meaning and can be used to retrieve content for custom question answering.",
                            }
                        },
                        "required": ["message"]
                    }
                }
            ]
        }
        termination_msg = lambda x: isinstance(x, dict) and "TERMINATE" == str(x.get("content", ""))[-9:].upper()

        # create an AssistantAgent instance named "assistant"


        boss = StreamlitUserProxyAgent(
            name="Boss",
            is_termination_msg=termination_msg,
            human_input_mode="NEVER",
            system_message="The boss who ask questions and give tasks.",
            code_execution_config=False,  # we don't want to execute code in this case.
            default_auto_reply="Reply `TERMINATE` if the task is done.",
        )
        # create a UserProxyAgent instance named "user"
        boss_aid = StreamlitRetrieveUserProxyAgent(
            name="Boss_Assistant",
            is_termination_msg=termination_msg,
            system_message="Assistant who has extra content retrieval power for solving difficult problems.",
            human_input_mode="NEVER",
            max_consecutive_auto_reply=3,
            retrieve_config={
                "task": "qa",
                "docs_path": "https://raw.githubusercontent.com/microsoft/autogen/main/README.md",
              #  "chunk_token_size": 1000,
              #  "model": config_list[0]["model"],
              #  "client": chromadb.PersistentClient(path="/tmp/chromadb"),
              #  "collection_name": "groupchat",
              #  "get_or_create": True,
            },
            code_execution_config=False,  # we don't want to execute code in this case.
        )
        noob = StreamlitAssistantAgent(
            name="assistant",
            system_message="You are a helpful noob assistant. reply 'TERMINATE' if the task is done.",
            llm_config=llm_config)

        def retrieve_content(message, n_results=3):
            boss_aid.n_results = n_results  # Set the number of results to be retrieved.
            # Check if we need to update the context.
            update_context_case1, update_context_case2 = boss_aid._check_update_context(message)
            if (update_context_case1 or update_context_case2) and boss_aid.update_context:
                boss_aid.problem = message if not hasattr(boss_aid, "problem") else boss_aid.problem
                _, ret_msg = boss_aid._generate_retrieve_user_reply(message)
            else:
                ret_msg = boss_aid.generate_init_message(message, n_results=n_results)
            return ret_msg if ret_msg else message


        for agent in [boss]:
            # register functions for all agents.
            agent.register_function(
                function_map={
                    "retrieve_content": retrieve_content,
                }
            )

        groupchat = autogen.GroupChat(
            agents=[boss, noob], messages=[], max_round=12
        )
        manager = autogen.GroupChatManager(groupchat=groupchat, llm_config=llm_config)
        # Create an event loop
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        # Define an asynchronous function
        async def initiate_chat():
            await boss.a_initiate_chat(
                manager,
                message=user_input,
            )

        # Run the asynchronous function within the event loop
        loop.run_until_complete(initiate_chat())
